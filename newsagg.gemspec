# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'newsagg/version'

Gem::Specification.new do |spec|
  spec.name          = 'newsagg'
  spec.version       = Newsagg::VERSION
  spec.authors       = ['Johannes Müller']
  spec.email         = ['straightshoota@gmail.com']

  spec.summary       = 'Parse and aggregate feed sources.'
  spec.description   = 'Parse and aggregate feed sources.'
  spec.homepage      = ''

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = ''
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.13'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'

  spec.add_dependency 'feedjira', '~>2.1.2'
  spec.add_dependency 'pastel', '~>0.7.1'
  spec.add_dependency 'sanitize', '~>4.4.0'
  spec.add_dependency 'i18n', '~>0.8.1'
  spec.add_dependency 'pry', '~>0.10.4'
end
