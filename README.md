# Newsagg

Newsagg aggregates newsfeeds from various sources.


## Usage

Create a config file `config.yml`:

```
site:
  title: Newsagg
  template_files: [index.html]
  #locale: en
  #template_folder: ./templates
  #output_folder: ./public

site1:
  title: "Site Title"
  link: "https://example.com"
  feed: "https://example.com/feed"

site2:
  title: "other Site"
  link: "https://www.example.com"
  feed: "https://www.example.com/atom.xml"

# ...
```

And a template in `template/index.html`.

```
<!DOCTYPE html>
<html>
  <head>
   <meta charset="utf-8">
   <meta name="generator" content="newsagg <%= Newsagg::VERSION %>">
   <title><%= site.title %></title>
  </head>
  <body>
    <h1>
      <%= site.title %>
    </h1>
    <% ItemCursor.new( entries.take(48) ).each do |entry, new_date, new_feed| %>

    <% if new_date %>
     <h2>
      <%= localize entry.published, '%A, %d. %B %Y' %>
     </h2>
    <% end %>

    <article class="entry">
        <%= link_to entry.feed.title, entry.feed.url %>
        <a href="<%= entry.url %>" title="<%= entry.short_summary %>">
            <%= sanitize entry.title %>
        </a>

        <%= sanitize entry.summary %>
    </article>
<% end %>
</body>
</html>

```


Then run `bundle exec newsagg config.yml`
