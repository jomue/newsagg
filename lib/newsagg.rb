require 'newsagg/version'
require 'newsagg/config'
require 'newsagg/parser'
require 'newsagg/feed'
require 'newsagg/entry'
require 'newsagg/template'
require 'newsagg/util/item_cursor'

module Newsagg
end
