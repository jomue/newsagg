require 'erb'
require 'fileutils'
require 'sanitize'
require 'i18n'

module Newsagg
  class Template
    TEMPLATE_FOLDER = 'template'.freeze
    OUTPUT_FOLDER   = 'public'.freeze

    attr_accessor :file, :template_folder, :output_folder, :logger

    def initialize(file, config, logger)
      @file = file
      @config = config
      @logger = logger
      @template_folder = config.site['template_folder'] || TEMPLATE_FOLDER
      @output_folder   = config.site['output_folder'] || OUTPUT_FOLDER
      I18n.locale = config.site['locale'] || 'en'
    end

    def extension
      '.erb'
    end

    def template
      @template ||= begin
        file_name = File.join(template_folder, file + extension)
        erb = ERB.new(File.read(file_name))
        erb.filename = file_name
        erb
      end
    end

    def render(data)
      site = DataStruct.new data['config'].site
      sources = DataStruct.new data['config'].sources
      root_path = 'template'
      entries = data['entries']
      feeds = data['feeds']

      b = binding

      template.result(b)
    end

    def render_and_save(data)
      result = render(data)
      save_output(result)
      result
    end

    def save_output(result)
      file_name = File.join(output_folder, file)
      FileUtils.mkdir_p(File.dirname(file_name))
      File.open(file_name, 'w') { |file| file.write(result) }

      logger.info "Saved content to #{pastel.cyan(file_name)}"
    end

    def link_to(text, url)
      %(<a href="#{url}">#{text}</a>)
    end

    def sanitize(html)
      Sanitize.fragment(html, elements: [])
    end

    def localize(input, format = nil)
      input = DateTime.parse(input) if input.is_a? String
      format = format =~ /^:(\w+)/ ? Regexp.last_match(1).to_sym : format
      I18n.l input, format: format
    end

    def self.load_translations
      if I18n.backend.send(:translations).empty?
        locales_glob = '../../locales/*.yml'
        I18n.backend.load_translations Dir[File.join(File.dirname(__FILE__), locales_glob)]
      end
    end

    load_translations

    private

    def pastel
      @pastel = Pastel.new
    end

    class DataStruct
      def initialize(data)
        @data = data
      end

      def method_missing(method, *args, &block)
        if @data.key?(method.to_s)
          @data[method.to_s]
        else
          super
        end
      end

      def respond_to_missing?(method)
        if @data.key?(method.to_s)
          true
        else
          super
        end
      end
    end
  end
end
