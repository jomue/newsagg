require 'yaml'
module Newsagg
  class Config
    def self.load(file)
      new(File.basename(file, '.*'), YAML.load_file(file))
    end

    attr_reader :site, :sources, :name

    def initialize(name, data = {})
      @name = name
      @site = data.delete('site') || {}
      @sources = data
      @sources.each_key do |id|
        @sources[id]['id'] = id
      end
    end
  end
end
