module Newsagg
  class ItemCursor
    def initialize(entries)
      @entries = entries
    end

    def each
      last_updated   = Time.local(1971, 1, 1)
      last_feed      = nil

      @entries.each do |entry|
        entry_updated = entry.updated # cache updated value ref

        new_date = last_updated.year != entry_updated.year ||
                   last_updated.month != entry_updated.month ||
                   last_updated.day   != entry_updated.day

        new_feed = new_date || last_feed != entry.feed

        yield(entry, new_date, new_feed)

        last_updated = entry.updated
        last_feed = entry.feed
      end
    end
  end
end
