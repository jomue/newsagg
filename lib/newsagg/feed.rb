module Newsagg
  class Feed
    # standard attributes:
    DEFAULT_ATTRIBUTES = %w(feed_url links url title description entries etag last_modified).freeze
    attr_accessor :attributes
    attr_accessor :entries, :id
    attr_accessor :feed_url, :url, :title, :favicon

    def initialize(attributes = {})
      @attributes = attributes
      @entries = []
    end

    def attributes
      @attributes || {}
    end

    def attribute_names
      DEFAULT_ATTRIBUTES + attributes.keys
    end

    def method_missing(method, *args, &block)
      if attribute_names.include?(method.to_s)
        @attributes[method.to_s]
      elsif attribute_names.include?(method.to_s.chomp('='))
        @attributes[method.to_s.chomp('=')] = args.first
      else
        super
      end
    end

    def respond_to_missing?(method, include_all = false)
      if attribute_names.include?(method.to_s) || attribute_names.include?(method.to_s.chomp('='))
        true
      else
        super
      end
    end

    def self.fetched_to_attributes(fetched)
      vars = fetched.instance_variables
      return new if vars.nil?
      attributes = vars.map do |attr|
        attr = attr.to_s[1..-1]
        next if attr == 'entries'
        val = fetched.send(attr)
        [attr, val]
      end
      Hash[attributes.compact]
    end
  end
end
