require 'yaml'
require 'pastel'
require 'logger'
require 'fileutils'

module Newsagg
  class CLI
    attr_reader :parser, :config, :errors

    def initialize(config)
      config = Config.load(config) unless config.is_a?(Config)
      @config = config
      @errors = []

      @parser = Newsagg::Parser.new(config, logger, errors)
    end

    def cache_dir
      config.site['cache_dir'] || '.cache'
    end

    def cache_file
      File.join(cache_dir, config.name + '.feeds.yml')
    end

    def fetch
      cache(cache_file) do
        feeds = parser.run

        handle_errors unless errors.empty?
        feeds
      end
    end

    def cache(cache_file)
      logger.debug "initialize cache for #{cache_file}"
      if File.exist?(cache_file)
        load_cache cache_file
      else
        logger.debug 'cache file does not exists, creating new'
        save_cache cache_file, yield
      end
    end

    def save_cache(cache_file, data)
      logger.debug "writing cache #{pastel.cyan(cache_file)}"
      FileUtils.mkdir_p(File.dirname(cache_file))
      File.open(cache_file, 'w') do |file|
        file.write YAML.dump(data)
      end
      data
    end

    def load_cache(cache_file)
      logger.info "loding cache from #{pastel.cyan(cache_file)}"
      YAML.load_file(cache_file)
    end

    def build(feeds)
      data = {}
      data['config'] = config
      data['entries'] = merge_entries(feeds)
      data['feeds'] = feeds.values

      data
    end

    def template(data)
      config.site['template_files'].each do |file|
        begin
          logger.info "Rendering #{pastel.cyan(file)}..."
          template = Template.new(file, config, logger)
          template.render_and_save(data)
        rescue StandardError => exc
          logger.error exc.message
        end
      end
    end

    def merge_entries(feeds)
      feeds.map { |_, feed| feed.entries }
           .flatten
           .delete_if { |entry| entry.published > Time.new }
           .sort { |a, b| b.published <=> a.published }
           .each { |entry| entry.feed = feeds[entry.feed] }
    end

    def logger
      @logger ||= Logger.new(STDOUT).tap do |logger|
        logger.level = Logger::INFO
        logger.progname = 'newsagg'
        logger.formatter = proc do |severity, _datetime, progname, msg|
          color = { 'WARN' => :yellow, 'ERROR' => :red }.fetch(severity, :white)
          severity_s = pastel.decorate(severity, color)
          "#{severity_s.ljust(6)} #{progname} #{msg}\n"
        end
      end
    end

    def handle_errors
      error_file = "newsagg.#{config.name}.error.log"
      logger.warn pastel.bright_red("!!!!! Encountered #{errors.size} errors:")
      File.open(error_file, 'w+') do |file|
        errors.each do |error|
          exc = error[:exception]
          title = "#{error[:source]}: #{exc.class.name}. "
          cause = "#{exc.cause.class.name}: #{exc.cause.message}" if exc.cause
          logger.warn "#{title} #{pastel.red(cause)}"

          file.puts "## #{title} ##"
          file.puts exc.message
          file.puts "#{exc.cause.class.name} #{exc.cause.message}" if exc.cause
          file.puts ''
          file.puts exc.backtrace.join("\n")
          file.puts ''
          file.puts ''
        end
      end
      logger.warn "!!!!! Details written to #{pastel.bright_red(error_file)}"
      @errors = []
    end

    private

    def pastel
      @pastel = Pastel.new
    end
  end
end
