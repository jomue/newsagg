module Newsagg
  class Entry
    # standard attributes:
    DEFAULT_ATTRIBUTES = %w(title url links published updated entry_id content author categories summary image).freeze
    attr_accessor :attributes, :feed

    def initialize(attributes = {})
      @attributes = attributes
    end

    def title
      @attributes.fetch('title') { short_summary }
    end

    def summary
      @attributes.fetch('summary') { content }
    end

    def short_summary
      summary[0..45] + '…'
    end

    def content
      @attributes.fetch('content', '')
    end

    def url
      @attributes['url'] || @attributes.fetch('links').first
    end

    def published
      @attributes.fetch('published')
    end

    def updated
      @attributes.fetch('updated') { published }
    end

    def attribute_names
      DEFAULT_ATTRIBUTES + attributes.keys
    end

    def attributes
      @attributes || {}
    end

    def method_missing(method, *args, &block)
      if attribute_names.include?(method.to_s)
        @attributes[method.to_s]
      elsif attribute_names.include?(method.to_s.chomp('='))
        @attributes[method.to_s.chomp('=')] = args.first
      else
        super
      end
    end

    def respond_to_missing?(method, include_private = false)
      if attribute_names.include?(method.to_s) || attribute_names.include?(method.to_s.chomp('='))
        true
      else
        super
      end
    end

    def self.from(fetched)
      vars = fetched.instance_variables
      return new if vars.nil?
      attributes = vars.map do |attr|
        attr = attr.to_s[1..-1]
        val = fetched.send(attr)
        [attr, val]
      end
      attributes = Hash[attributes.compact] unless attributes.nil?
      attributes ||= {}
      new(attributes)
    end
  end
end
