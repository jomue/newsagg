require 'feedjira'
require 'pastel'

Feedjira::Feed.add_common_feed_entry_element('media:thumbnail', as: :image, value: :url)

module Newsagg
  class Parser
    attr_reader :config, :errors, :logger

    def initialize(config, logger, errors)
      config = Config.load(config) unless config.is_a?(Config)
      @config = config
      @logger = logger
      Feedjira.logger = logger.dup.tap do |l|
        l.progname = 'Feedjira'
      end
      @errors = errors
    end

    def run
      feeds = {}

      config.sources.each do |id, source|
        feed = Feed.new
        feed.url = source['link']
        feed.feed_url = source['feed']
        feed.title = source['title']
        feed.id = source['id']
        feed.favicon = source['favicon']

        logger.info "fetching #{pastel.cyan(source['feed'])}"
        parse_source(feed)
        next if feed.nil?
        num_entries = pastel.green("#{feed.entries.size} entries")
        logger.info "parsed #{num_entries} from #{pastel.yellow(feed.title)}"
        feeds[id] = feed
      end

      feeds
    end

    def parse_source(feed)
      fetched = Feedjira::Feed.fetch_and_parse feed.feed_url

      feed.attributes = Feed.fetched_to_attributes(fetched)

      fetched.entries.each do |entry_data|
        entry = Entry.from(entry_data)
        entry.feed = feed.id
        feed.entries << entry
      end
    rescue Faraday::ClientError, Feedjira::FetchFailure, Feedjira::NoParserAvailable => exc
      name = exc.cause ? exc.cause.class.name : exc.class.name
      logger.error "#{pastel.yellow(feed.title)} #{name}: #{exc.message}"
      errors << { source: feed.id, exception: exc }
    end

    private

    def pastel
      @pastel = Pastel.new
    end
  end
end
